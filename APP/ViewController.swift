//
//  ViewController.swift
//  APP
//
//  Created by Szasz Karina on 25/06/2019.
//  Copyright © 2019 Szasz Karina. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    

    @IBAction func showMessage(sender : UIButton){
        var emojiDict : [String : String]=["🐶": "Dog Emoji",
                                          "🐱": "Cat Emoji",
                                          "🦁": "Lion Emoji",
                                          "🐷": "Pig Emoji",
                                          "🦄": "Unicorn Emoji"
        ]
        let selectedButton = sender
        var wordToLookup="🐶"
        var meaning=emojiDict[wordToLookup]
        wordToLookup="🐱"
        meaning=emojiDict[wordToLookup]
        wordToLookup="🦄"
        meaning=emojiDict[wordToLookup]
        wordToLookup="🐷"
        meaning=emojiDict[wordToLookup]
        wordToLookup="🦁"
    
        if let wordToLookup=selectedButton.titleLabel?.text{
        
            meaning=emojiDict[wordToLookup]
            let alertController = UIAlertController(title: "Meaning", message: meaning, preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }



}

